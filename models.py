
import torch
import torch.nn as nn
from torch.autograd import Variable
import random
import torch.nn.functional as F

# MODEL MODES
# RNN_BASE='RNN_BASE' # Basic RNN that only uses input items (no context used)
TC = 'TC' # Temporal Context as input (No RNN, refer to diagram)
STAR = 'STAR' # Temporal Context as input (with RNN, refer to diagram) using h3
IC = 'IC' # Input Context as input (No RNN, refer to diagram)
SIAR = 'SIAR' # Input Context as input (With RNN, refer to diagram)
SITAR= 'SITAR' # Combine STAR and SIAR (refer to diagram)
DHAR = 'DHAR' # Dual Hybrid Context RNN(both temporal and input context)
HAR = 'HAR' # Hybrid Context RNN(both temporal and input context) that splits the RNN into 2 contextual weights

class SRNNModel(nn.Module):
	
	def __init__(self,mode=None,num_class=2,hidden_size=40,hour_size=24,weekday_size=7,isCuda=False):
		super(SRNNModel, self).__init__()
		self.isCuda = isCuda
		self.num_class = num_class
		self.weekday_size=weekday_size
		self.hour_size=hour_size
		# self.num_layers = num_layers
		# self.input_size = input_size
		self.hidden_size = hidden_size
		# self.sequence_length = sequence_length
		self.batch_size = 1
		self.mode = mode
		if mode == STAR:
			# (inputSize which is size of dictionary,embedding vector size)
			self.inputEmbedding = nn.Embedding(self.num_class, self.hidden_size)
			# 1st layer params for interval context rnn
			# use hiddenLayer contains weights and bias for temporal hidden layer (h3)
			self.LinearH3 = nn.Linear(self.hidden_size,self.hidden_size)

			# Weight and bias for interval context
			self.WIntervalX = torch.nn.Parameter(torch.randn(self.hidden_size,1),requires_grad=True)
			self.BIntervalX = torch.nn.Parameter(torch.randn(self.hidden_size),requires_grad=True)

			# 2nd layer for interval context rnn
			self.Linear2ndIntervalLayer = nn.Linear(self.hidden_size,self.hidden_size*self.hidden_size)
			
			# Used for interval context
			self.BIntervalContext = torch.nn.Parameter(torch.randn(self.hidden_size),requires_grad=True)
			# Weight and bias parameter for x which is the input
			self.Wx = torch.nn.Parameter(torch.randn(self.hidden_size,self.hidden_size),requires_grad=True)
			self.Bx = torch.nn.Parameter(torch.randn(self.hidden_size),requires_grad=True)

			self.fc = nn.Linear(self.hidden_size,self.num_class)
		
		elif mode == SIAR:
			# (inputSize which is size of dictionary,embedding vector size)
			self.inputEmbedding = nn.Embedding(self.num_class, self.hidden_size)

			# 1st Layer params for input context rnn
			# Context Embedding (of size (1,D*D))
			self.hourEmbedding = nn.Embedding(hour_size, self.hidden_size)
			self.weekdayEmbedding = nn.Embedding(weekday_size, self.hidden_size)

			self.LinearH2 = nn.Linear(self.hidden_size,self.hidden_size)

			# Used to generate weights for input context
			self.LinearHourWeekdayX = nn.Linear(self.hidden_size*2,self.hidden_size)

			# 2nd Layer params for input context rnn
			self.Linear2ndInputContextLayer = nn.Linear(self.hidden_size,self.hidden_size*self.hidden_size)

			self.BInputContext = torch.nn.Parameter(torch.randn(self.hidden_size),requires_grad=True)

			# Weight and bias parameter for h
			self.Wh = torch.nn.Parameter(torch.randn(self.hidden_size,self.hidden_size),requires_grad=True)
			self.Bh = torch.nn.Parameter(torch.randn(self.hidden_size),requires_grad=True)

			self.fc = nn.Linear(self.hidden_size,self.num_class)
		elif mode == SITAR:
			# (inputSize which is size of dictionary,embedding vector size)
			self.inputEmbedding = nn.Embedding(self.num_class, self.hidden_size)
			#### INPUT CONTEXT #######
			# 1st Layer params for input context rnn
			# Context Embedding (of size (1,D*D))
			self.hourEmbedding = nn.Embedding(hour_size, self.hidden_size)
			self.weekdayEmbedding = nn.Embedding(weekday_size, self.hidden_size)

			self.LinearH2 = nn.Linear(self.hidden_size,self.hidden_size)

			# Used to generate weights for input context
			self.LinearHourWeekdayX = nn.Linear(self.hidden_size*2,self.hidden_size)

			# 2nd Layer params for input context rnn
			self.Linear2ndInputContextLayer = nn.Linear(self.hidden_size,self.hidden_size*self.hidden_size)

			self.BInputContext = torch.nn.Parameter(torch.randn(self.hidden_size),requires_grad=True)

			#### INTERVAL CONTEXT #######
			# 1st layer params for interval context rnn
			# use hiddenLayer contains weights and bias for temporal hidden layer (h3)
			self.LinearH3 = nn.Linear(self.hidden_size,self.hidden_size)

			# Weight and bias for interval context
			self.WIntervalX = torch.nn.Parameter(torch.randn(self.hidden_size,1),requires_grad=True)
			self.BIntervalX = torch.nn.Parameter(torch.randn(self.hidden_size),requires_grad=True)

			# 2nd layer for interval context rnn
			self.Linear2ndIntervalLayer = nn.Linear(self.hidden_size,self.hidden_size*self.hidden_size)
			
			# Used for interval context
			self.BIntervalContext = torch.nn.Parameter(torch.randn(self.hidden_size),requires_grad=True)
			
			self.fc = nn.Linear(self.hidden_size,self.num_class)
		elif mode == HAR:
			# (inputSize which is size of dictionary,embedding vector size)
			self.inputEmbedding = nn.Embedding(self.num_class, self.hidden_size)

			self.hourEmbedding = nn.Embedding(hour_size, self.hidden_size)
			self.weekdayEmbedding = nn.Embedding(weekday_size, self.hidden_size)

			# Used to generate weights for input context
			self.LinearHourWeekdayX = nn.Linear(self.hidden_size*2,self.hidden_size)

			# Weight and bias for interval context
			self.WIntervalX = torch.nn.Parameter(torch.randn(self.hidden_size,1),requires_grad=True)
			self.BIntervalX = torch.nn.Parameter(torch.randn(self.hidden_size),requires_grad=True)

			self.LinearH2 = nn.Linear(self.hidden_size,self.hidden_size)

			# 2nd Layer params for input context rnn
			self.Linear2ndBeforeSplitLayer = nn.Linear(self.hidden_size,self.hidden_size*self.hidden_size*2)

			self.Bh = torch.nn.Parameter(torch.randn(self.hidden_size),requires_grad=True)
			self.Bx = torch.nn.Parameter(torch.randn(self.hidden_size),requires_grad=True)

			self.fc = nn.Linear(self.hidden_size,self.num_class)
		elif mode == TC:
			# (inputSize which is size of dictionary,embedding vector size)
			self.inputEmbedding = nn.Embedding(self.num_class, self.hidden_size)

			# Weight and bias for interval context
			self.WIntervalX = torch.nn.Parameter(torch.randn(self.hidden_size*self.hidden_size,1),requires_grad=True)
			self.BIntervalX = torch.nn.Parameter(torch.randn(self.hidden_size*self.hidden_size),requires_grad=True)
			# Used for interval context
			self.BIntervalContext = torch.nn.Parameter(torch.randn(self.hidden_size),requires_grad=True)
			# Weight and bias parameter for x which is the input
			self.Wx = torch.nn.Parameter(torch.randn(self.hidden_size,self.hidden_size),requires_grad=True)
			self.Bx = torch.nn.Parameter(torch.randn(self.hidden_size),requires_grad=True)

			self.fc = nn.Linear(self.hidden_size,self.num_class)
		elif mode == IC:
			# (inputSize which is size of dictionary,embedding vector size)
			self.inputEmbedding = nn.Embedding(self.num_class, self.hidden_size)
			# Context Embedding (of size (1,D*D))
			self.hourEmbedding = nn.Embedding(self.hour_size, self.hidden_size)
			self.weekdayEmbedding = nn.Embedding(self.weekday_size, self.hidden_size)

			# Used to generate weights for input context
			self.LinearHourWeekdayX = nn.Linear(self.hidden_size*2,self.hidden_size*self.hidden_size)

			self.BInputContext = torch.nn.Parameter(torch.randn(self.hidden_size),requires_grad=True)

			# Weight and bias parameter for h
			self.Wh = torch.nn.Parameter(torch.randn(self.hidden_size,self.hidden_size),requires_grad=True)
			self.Bh = torch.nn.Parameter(torch.randn(self.hidden_size),requires_grad=True)

			self.fc = nn.Linear(self.hidden_size,self.num_class)


	def forward(self, inputX,hourX,weekdayX,intervalX,h=None,h2=None,h3=None):

		if self.mode == STAR:
			# We use sigmoid as activation function
			actFunc = F.sigmoid
			# First run so initialise hidden layer
			if h is None:
				# h = torch.zeros(self.hidden_size,requires_grad=True)
				# h3 = torch.zeros(self.hidden_size,requires_grad=True)
				h = self.genGradZeroTensor(self.hidden_size)
				h3 = self.genGradZeroTensor(self.hidden_size)

			xEmbedded = self.inputEmbedding(inputX)
			# NOTE: h3 has a size of (1*D) and is the output of the first layer of context rnn
			h3 = actFunc(F.linear(intervalX.unsqueeze(0),self.WIntervalX,self.BIntervalX)
						+self.LinearH3(h3))
			# NOTE: we pass h3 into 2nd layer of context rnn to generate weight matrix of size (D,D)
			intervalContext = actFunc(self.Linear2ndIntervalLayer(h3)).reshape(self.hidden_size,self.hidden_size)
		
			h = actFunc(F.linear(xEmbedded,self.Wx,self.Bx)\
					+F.linear(h,intervalContext,self.BIntervalContext))

			logits =  self.fc(h)
			return logits,h,h2,h3

		elif self.mode == SIAR:
			# We use sigmoid as activation function
			actFunc = F.sigmoid
			# First run so initialise hidden layer
			if h is None:
				# h = torch.zeros(self.hidden_size,requires_grad=True)
				# h2 = torch.zeros(self.hidden_size,requires_grad=True)
				h = self.genGradZeroTensor(self.hidden_size)
				h2 = self.genGradZeroTensor(self.hidden_size)
				

			xEmbedded = self.inputEmbedding(inputX)
			hourEmbedded = self.hourEmbedding(hourX)
			weekdayEmbedded = self.weekdayEmbedding(weekdayX)

			h2 = actFunc(self.LinearHourWeekdayX(torch.cat((hourEmbedded,weekdayEmbedded)))
						 +self.LinearH2(h2))
			
			inputContext = actFunc(self.Linear2ndInputContextLayer(h2)).reshape(self.hidden_size,self.hidden_size)
		
			# act(x*WxT+Bx+h*WhT+Bh)
			h = actFunc(F.linear(xEmbedded,inputContext,self.BInputContext)\
					+F.linear(h,self.Wh,self.Bh))

			logits =  self.fc(h)
			return logits,h,h2,h3
		elif self.mode == SITAR:
			# We use sigmoid as activation function
			actFunc = F.sigmoid
			# First run so initialise hidden layer
			if h is None:
				# h = torch.zeros(self.hidden_size,requires_grad=True)
				# h2 = torch.zeros(self.hidden_size,requires_grad=True)
				# h3 = torch.zeros(self.hidden_size,requires_grad=True)
				h = self.genGradZeroTensor(self.hidden_size)
				h2 = self.genGradZeroTensor(self.hidden_size)
				h3 = self.genGradZeroTensor(self.hidden_size)

			xEmbedded = self.inputEmbedding(inputX)
			hourEmbedded = self.hourEmbedding(hourX)
			weekdayEmbedded = self.weekdayEmbedding(weekdayX)

			h2 = actFunc(self.LinearHourWeekdayX(torch.cat((hourEmbedded,weekdayEmbedded)))
						 +self.LinearH2(h2))
			
			inputContext = actFunc(self.Linear2ndInputContextLayer(h2)).reshape(self.hidden_size,self.hidden_size)

			# NOTE: h3 has a size of (1*D) and is the output of the first layer of context rnn
			h3 = actFunc(F.linear(intervalX.unsqueeze(0),self.WIntervalX,self.BIntervalX)
						+self.LinearH3(h3))
			# NOTE: we pass h3 into 2nd layer of context rnn to generate weight matrix of size (D,D)
			intervalContext = actFunc(self.Linear2ndIntervalLayer(h3)).reshape(self.hidden_size,self.hidden_size)
		
		
			# act(x*WxT+Bx+h*WhT+Bh)
			h = actFunc(F.linear(xEmbedded,inputContext,self.BInputContext)\
					+F.linear(h,intervalContext,self.BIntervalContext))

			logits =  self.fc(h)
			return logits,h,h2,h3
		elif self.mode == HAR:
			# We use sigmoid as activation function
			actFunc = F.sigmoid
			# First run so initialise hidden layer
			if h is None:
				# h = torch.zeros(self.hidden_size,requires_grad=True)
				# h2 = torch.zeros(self.hidden_size,requires_grad=True)
				h = self.genGradZeroTensor(self.hidden_size)
				h2 = self.genGradZeroTensor(self.hidden_size)

			xEmbedded = self.inputEmbedding(inputX)
			hourEmbedded = self.hourEmbedding(hourX)
			weekdayEmbedded = self.weekdayEmbedding(weekdayX)

			h2 = actFunc(self.LinearHourWeekdayX(torch.cat((hourEmbedded,weekdayEmbedded)))
						 + F.linear(intervalX.unsqueeze(0),self.WIntervalX,self.BIntervalX)
						 + self.LinearH2(h2))
			
			beforeSplit = actFunc(self.Linear2ndBeforeSplitLayer(h2)).reshape(2,self.hidden_size,self.hidden_size)
			Wx, Wh = torch.chunk(beforeSplit,2,0)

			# # act(x*WxT+Bx+h*WhT+Bh)
			h = actFunc(F.linear(xEmbedded,Wx.squeeze(0),self.Bx)\
					+F.linear(h,Wh.squeeze(0),self.Bh))

			logits =  self.fc(h)
			return logits,h,h2,h3
		elif self.mode == TC:
			# We use sigmoid as activation function
			actFunc = F.sigmoid
			# First run so initialise hidden layer
			if h is None:
				# h = torch.zeros(self.hidden_size,requires_grad=True)
				h = self.genGradZeroTensor(self.hidden_size)
			

			xEmbedded = self.inputEmbedding(inputX)
			
			intervalContext = actFunc(F.linear(intervalX.unsqueeze(0),self.WIntervalX,self.BIntervalX)).reshape(self.hidden_size,self.hidden_size)
		
			# act(x*WxT+Bx+h*WhT+Bh)
			h = actFunc(F.linear(xEmbedded,self.Wx,self.Bx)\
					+F.linear(h,intervalContext,self.BIntervalContext))

			logits =  self.fc(h)
			return logits,h,h2,h3
		elif self.mode == IC:
			# We use sigmoid as activation function
			actFunc = F.sigmoid
			# First run so initialise hidden layer
			if h is None:
				# h = torch.zeros(self.hidden_size,requires_grad=True)
				h = self.genGradZeroTensor(self.hidden_size)

			xEmbedded = self.inputEmbedding(inputX)
			hourEmbedded = self.hourEmbedding(hourX)
			weekdayEmbedded = self.weekdayEmbedding(weekdayX)

			temp = torch.cat((hourEmbedded,weekdayEmbedded),0)
			# print('temp.size(): '+str(temp.size()))
			inputContext = actFunc(self.LinearHourWeekdayX(torch.cat((hourEmbedded,weekdayEmbedded)))).reshape(self.hidden_size,self.hidden_size)
			# print('inputContext: '+str(inputContext.size()))

			# intervalContext = intervalContext.reshape(self.hidden_size,self.hidden_size)
		
			# act(x*WxT+Bx+h*WhT+Bh)
			h = actFunc(F.linear(xEmbedded,inputContext,self.BInputContext)\
					+F.linear(h,self.Wh,self.Bh))
			# print('h.size(): '+str(h.size()))

			logits =  self.fc(h)
			# print('logits.size(): '+str(logits.size()))
			return logits,h,h2,h3

	def genGradZeroTensor(self,size,tensorType=None,requires_grad=True):
		# isCuda = usingCuda()
		if(tensorType==None and self.isCuda):
			# return torch.cuda.FloatTensor(size,requires_grad = requires_grad).fill_(0)
			# return torch.cuda.FloatTensor(size).fill_(0)
			return torch.zeros(self.hidden_size,requires_grad =requires_grad).cuda()
		elif tensorType == None:
			return torch.zeros(self.hidden_size,requires_grad =requires_grad)
			# return torch.zeros(size)

